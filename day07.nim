# https://adventofcode.com/2021/day/7
# Part 2 uses https://en.wikipedia.org/wiki/1_+_2_+_3_+_4_+_%E2%8B%AF

import os, strutils

var
  startingPositions: seq[int] = @[]
  maxPos: int
  
block readInput:
  if os.paramCount() < 1:
    quit(1)

  for value in readFile(os.paramStr(1)).strip().splitLines()[0].split(","):
    startingPositions.add(value.parseInt())
  
  maxPos = max(startingPositions)

# Minimum travel costs
block part1:
  var
    travelCosts: seq[int] = newSeq[int](maxPos)
  
  # travelCosts[<pos>] = cost to bring all submarines to position <pos>
    
  for startPos in startingPositions:
    for destination in travelCosts.low()..travelCosts.high():
      travelCosts[destination] += abs(destination - startPos)
  
  echo "Part 1: ", min(travelCosts)

# Minimum fuel costs
# Here, fuel cost increases from 1 to <distance_to_travel> 
# The sum of such a series is n*(n+1)/2 where n = <distance_to_travel>
block part2:
  var
    distance: int
    fuelCosts: seq[int] = newSeq[int](maxPos)
  
  # fuelCosts[<pos>] = fuel cost to bring all submarines to position <pos>
    
  for startPos in startingPositions:
    for destination in fuelCosts.low()..fuelCosts.high():
      distance = abs(destination - startPos)
      fuelCosts[destination] += (distance * (distance + 1) div 2)
  
  echo "Part 2: ", min(fuelCosts)