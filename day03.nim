# https://adventofcode.com/2021/day/3

import os, strutils, parseutils

if os.paramCount() < 1:
  quit(1)

let 
  data = readFile(os.paramStr(1)).strip().splitLines()
  bitcount = data[0].len()

block part1:
  var
    ones, zeros: int
    gamma, epsilon: string
    gammaVal, epsilonVal: int
    
  for i in 0 ..< bitcount:
    zeros = 0
    ones = 0
    for line in data:
      if line[i] == '0':
        zeros += 1
      else:
        ones += 1
    
    if zeros > ones:
      gamma &= '0'
      epsilon &= '1'
    else:
      gamma &= '1'
      epsilon &= '0'
      
  discard gamma.parseBin(gammaVal)
  discard epsilon.parseBin(epsilonVal)
  echo "Part 1: ", gammaVal * epsilonVal
  
block part2:
  var
    o2Zeros, o2Ones, co2Zeros, co2Ones: int
    oxygen, co2: string
    oxygenVal, co2Val: int
    
  for i in 0..<bitcount:
    co2Zeros = 0
    co2Ones = 0
    o2Zeros = 0
    o2Ones = 0
    
    for line in data:
      if oxygen == "" or line[0..<i] == oxygen:
        if line[i] == '0':
          o2Zeros += 1
        else:
          o2Ones += 1
          
      if co2 == "" or line[0..<i] == co2:
        if line[i] == '0':
          co2Zeros += 1
        else:
          co2Ones += 1
    
    if (o2Zeros + o2Ones) == 1:
      # Only one eligible value
      if o2Ones > 0:
        oxygen &= '1'
      else:  
        oxygen &= '0'
    else:
      if o2Zeros > o2Ones:
        oxygen &= '0'
      else:
        oxygen &= '1'
      
    if (co2Zeros + co2Ones) == 1: 
      # Only one eligible value
      if co2Ones > 0:
        co2 &= '1'
      else:
        co2 &= '0'
    else:
      if co2Zeros <= co2Ones:
        co2 &= '0'
      else:
        co2 &= '1'
      
  discard oxygen.parseBin(oxygenVal)
  discard co2.parseBin(co2Val)
  echo "Part 2: ", oxygenVal * co2Val