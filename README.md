# Advent of Code 2021 Solutions

## About
This repository contains the solutions for the puzzles in the [2021 Advent of Code calendar](https://adventofcode.com/2021)

The solutions are in the [Nim](https://nim-lang.org) programming language.

There is one file for each day. The files are named `day01.nim`, `day02.nim` and so on.

For all the programs, the file containing the puzzle input needs to be specified on the command line.

While the event is running, the solution will be committed to this repository after the deadline for that puzzle has expired or a working solution is found, whichever is later.

The code shared here may be slightly different than what was used to actually solve the puzzle, as I may have refactored it or added additional comments.

## Compiling and Running
1. [Install](https://nim-lang.org/install.html) Nim
2. To compile and run a file, issue a command similar to the one below (see `nim --fullhelp` for all available options)  
`nim r -d:release dayxx.nim dayxx.txt`  
where `dayxx.nim` is the Nim source file and `dayxx.txt` is the puzzle input
