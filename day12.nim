# https://adventofcode.com/2021/day/12

# Here, part 1 can be thought of as a special case of part 2, where the number
# of small caves that can be visited twice is 0.

import os, strutils

type
  # Here, caves contains the names of the caves and paths is a square array
  # which contains the path details, i.e.
  # caves[i] = name of the cave
  # paths[i][j] = true if path exists from cave[i] to cave[j]
  Map = tuple
    caves: seq[string]
    paths: seq[seq[bool]]

# Find all paths from start to end, allowing for a specified number of small 
# caves to be visited twice
proc findPaths(map: Map, smallCavesToVistTwice: int = 0): int =
  var
    pathsToExplore: seq[seq[int]] = @[]
    smallCavesVisitedTwice: seq[seq[int]] = @[]
    startIndex, endIndex, currentIndex: int
    currentPath, currentSmallVisitedTwice: seq[int]
    
  startIndex = map.caves.find("start")
  endIndex = map.caves.find("end")
  result = 0
  
  # Initialize with starting data
  pathsToExplore.add(@[startIndex])
  smallCavesVisitedTwice.add(@[])
  
  while pathsToExplore.len() > 0:
    var
      isSecondVisitToSmallCave: bool
    
    # Get details of path to be explored
    currentPath = pathsToExplore[0]
    currentSmallVisitedTwice = smallCavesVisitedTwice[0] 
    pathsToExplore.del(0)
    smallCavesVisitedTwice.del(0)
    
    currentIndex = currentPath[currentPath.high()]
    
    # Add caves for checking
    for cave in map.paths[currentIndex].low()..map.paths[currentIndex].high():
      # The starting cave can be visited only once
      if cave == startIndex:
        continue
      
      if map.paths[currentIndex][cave]:
        # Check for direct path to exit
        if cave == endIndex:
          result.inc()
          continue
        
        # For small cave check if it can be visited twice
        if map.caves[cave].toLowerAscii() == map.caves[cave] and 
          cave in currentPath:
          if (currentSmallVisitedTwice.len() == smallCavesToVistTwice) or
            cave in currentSmallVisitedTwice:
            continue
          else:
            isSecondVisitToSmallCave = true
        else:
          isSecondVisitToSmallCave = false
        
        # Store details of the updated path that needs to be checked
        pathsToExplore.add(currentPath)
        pathsToExplore[pathsToExplore.high()].add(cave)
        
        smallCavesVisitedTwice.add(currentSmallVisitedTwice)
        # Add current cave to list if this is the second visit
        if isSecondVisitToSmallCave:
          smallCavesVisitedTwice[smallCavesVisitedTwice.high()].add(cave)

var
  map: Map

block readInput:
  if os.paramCount() < 1:
    quit(1)
    
  let
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  map.caves = @[]
  map.paths = @[]
  
  var 
    caves: seq[string]
    srcIndex, destIndex: int
  
  # First pass over input file to collect names of the caves
  for line in data:
    for cave in line.split("-"):
      if map.caves.find(cave) == -1:
        map.caves.add(cave)
  
  # Initialize a square grid of the cave names to store the paths
  map.paths = newSeq[seq[bool]](map.caves.len())
  for i in map.caves.low()..map.caves.high():
    map.paths[i] = newSeq[bool](map.caves.len())
  
  # Second pass over input file to update path details
  for line in data:
    caves = line.split("-")
    srcIndex = map.caves.find(caves[0])
    destIndex = map.caves.find(caves[1])
    
    # As travel in either direction is possible
    map.paths[srcIndex][destIndex] = true
    map.paths[destIndex][srcIndex] = true
    
# All paths where small caves are visited only once  
block part1:
  echo "Part 1: ", map.findPaths()
  
# At least one small cave can be visited twice
block part2:
  echo "Part 2: ", map.findPaths(1)