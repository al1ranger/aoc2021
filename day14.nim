# https://adventofcode.com/2021/day/14
# Useful hints at 
# https://www.reddit.com/r/adventofcode/comments/rfzq6f/2021_day_14_solutions/

# The trick here is to keep track of the pairs instead of building the entire
# polymer. This helps to keep the memory requirements low without affecting
# performance

import os, strutils, tables

type
  PolymerDetails = tuple
    pairs: Table[string, int]
    elementCounts: Table[char, int]
  
  Rules = Table[string, char]

# Executes a step of the substitution  
proc executeStep(details: var PolymerDetails, rules: Rules) =
  var
    newPair: string
    oldCounts: Table[string, int]
  
  # Because a split can generate an existing pair, we need to store the counts
  # from before the split. We can ignore pairs whose count is zero.
  for pair in details.pairs.keys():
    if details.pairs[pair] > 0:
      discard oldCounts.hasKeyOrPut(pair, details.pairs[pair])
  
  # Determine which rules match and create the new pairs
  for pair in oldCounts.keys:
    if rules.hasKey(pair):
      # Create the two new pairs and increment their counts
      newPair = pair[0] & rules[pair]
      if details.pairs.hasKeyOrPut(newPair, oldCounts[pair]):
        details.pairs[newPair].inc(oldCounts[pair])
      
      newPair = rules[pair] & pair[1]
      if details.pairs.hasKeyOrPut(newPair, oldCounts[pair]):
        details.pairs[newPair].inc(oldCounts[pair])
      
      # Increment the count for the newly added letter
      if details.elementCounts.hasKeyOrPut(rules[pair], oldCounts[pair]):
        details.elementCounts[rules[pair]].inc(oldCounts[pair])
      
      # Decrement the count for the old pair
      details.pairs[pair] -= oldCounts[pair]

# Gets the difference between the maximum and minimum letter counts
proc getDifference(details: PolymerDetails): int =
  result = 0
  
  var
    max, min: int
  
  max = 0
  min = high(int)
  
  for count in details.elementCounts.values():
    if count > max:
      max = count
    
    if count < min:
      min = count
      
  result = max - min

var
  polymerDetails: PolymerDetails
  rules: Rules

block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  for line in readFile(os.paramStr(1)).strip().splitLines():
    if line == "":
      continue
    
    # The polymer comes first
    if polymerDetails.pairs.len() == 0:
      var
        start, finish: int
      
      start = 0
      finish = start + 1
      while finish <= line.high():
        # Add the pair and letter counts
        if polymerDetails.pairs.hasKeyOrPut(line[start..finish], 1):
          polymerDetails.pairs[line[start..finish]].inc()
          
        # Except the first iteration, the first letter of the pair is the last 
        # letter of the previous iteration
        if start == 0 and 
          polymerDetails.elementCounts.hasKeyOrPut(line[start], 1):
          polymerDetails.elementCounts[line[start]].inc()
        
        if polymerDetails.elementCounts.hasKeyOrPut(line[finish], 1):
          polymerDetails.elementCounts[line[finish]].inc()
        
        # Increment positions
        start.inc()
        finish.inc()
            
      continue
    
    # And then the rules
    var
      parts: seq[string]
    
    parts = line.split(" -> ")  
    
    if rules.hasKeyOrPut(parts[0], parts[1][0]):
      rules[parts[0]] = parts[1][0]
    
# Difference between maximum and minimum letter counts after 10 steps  
block part1:
  for step in 1..10:
    polymerDetails.executeStep(rules)
  
  echo "Part 1: ", polymerDetails.getDifference() 
  
# Difference between maximum and minimum letter counts after 40 steps  
block part1:
  for step in 11..40:
    polymerDetails.executeStep(rules)
  
  echo "Part 2: ", polymerDetails.getDifference() 