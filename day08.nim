# https://adventofcode.com/2021/day/8

import os, strutils, algorithm

type
  Reading = tuple
    patterns: seq[string]
    digits: seq[string]
  
  Readings = seq[Reading]

var
  readings: Readings = @[]

# While storing the segment details for the inputs, sort them alphabetically
# to simplify comparison. For the signal patterns, also consider length while
# sorting so that the first three are always 1, 7, and 4  
block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  var
    data: seq[string]
    reading: Reading
  
  for line in readFile(os.paramStr(1)).strip().splitLines():
    data = line.split("|")
    
    reading = (@[], @[])
    
    for pattern in data[0].strip().split(" "):
      reading.patterns.add(pattern.sorted(system.cmp[char]).join())
    reading.patterns.sort do (x,y: string) -> int:
      result = cmp(x.len(), y.len())
      if result == 0:
        result = cmp(x, y)
    
    for digit in data[1].strip().split(" "):
      reading.digits.add(digit.sorted(system.cmp[char]).join())
    
    readings.add(reading)

# Occurences of 1 (2 segments), 4 (4 segments), 7 (3 segments), 8 (7 segments)
block part1:
  var
    length: int
    count: int = 0
  
  for reading in readings:
    for digit in reading.digits:
      length = digit.len()
      if length == 2 or length == 3 or length == 4 or length == 7:
        count.inc()
  
  echo "Part 1: ", count


# The positions 1 to 7 indicate the segments as below
#  1111
# 2    3
# 2    3
#  4444
# 5    6
# 5    6
#  7777

# Here we use 1, 4 and 7 to restrict the possible signals for each segment.
# As we are left with one possibility for segment 1 and two possibilities
# for the remaining six, a maximum of 32 combinations are possible. However, as
# each signal must appear only once, the actual number is even lesser.
# As a result, a brute force approach is viable.
block part2:
  type
    Segments = array[1..7, seq[char]]
  
  # Helper procedure to remove a signal from the list of possibilities
  proc removeCandidate(segments: var Segments, position: int, signal: char) =
    var
      index: int = segments[position].find(signal)

    if index != -1:
      segments[position].del(index)
  
  # Helper procedure to add a signal to the list of possibilities
  proc addCandidate(segments: var Segments, position: int, signal: char) =
    var
      index: int = segments[position].find(signal)

    if index == -1:
      segments[position].add(signal)

  # Helper procedure to set the specified segment to a specific signal and 
  # remove that signal from the list of possibilities for others
  proc setValue(segments: var Segments, position: int, signal: char) =
    segments[position] = @[]
    
    var
      index: int
    
    for i in segments.low() .. segments.high():
      index = segments[i].find(signal)
      
      if i != position:
        if index != -1:
          segments[i].del(index)
      else:
        if index == -1:
          segments[i].add(signal)
  
  # Calculate the total
  var
    total: int = 0
  
  for reading in readings:
    var
      segments: Segments
    
    # Initially, all possibilities exist
    for i in 1..7:
      segments[i] = @['a', 'b', 'c', 'd', 'e', 'f', 'g']
    
    # Use 1, 4, 7, 8 to restrict the possibilities
    # As patterns are sorted by length, first three will always be 1, 7, 4
    # and last is always 8
    var
      one, seven, four: string
    
    one = reading.patterns[0]
    seven = reading.patterns[1]
    four = reading.patterns[2]
    
    for c in seven:
      if c notin one:
        segments.setValue(1, c)
    
    segments[3] = @[]
    segments[6] = @[]
    for c in one:
      for i in 1..7:
        if i != 3 and i != 6:
          segments.removeCandidate(i, c)
        else:
          segments.addCandidate(i, c)
    
    segments[2] = @[]
    segments[4] = @[]
    for c in four:
      if c notin one:
        for i in 1..7:
          if i != 2 and i != 4:
            segments.removeCandidate(i, c)
          else:
            segments.addCandidate(i, c)
    
    # Now that the constraints are applied, build a list of possible 
    # combinations
    var
      values: array[0..9, string]
      combinations: seq[array[0..9, string]] = @[]
      
    for c1 in segments[1]:
      for c2 in segments[2]:
        if c2 == c1:
          continue
          
        for c3 in segments[3]:
          if c3 == c2 or c3 == c1:
            continue
          
          for c4 in segments[4]:
            if c4 == c1 or c4 == c2 or c4 == c3:
              continue
            
            for c5 in segments[5]:
              if c5 == c1 or c5 == c2 or c5 == c3 or c5 == c4:
                continue
              
              for c6 in segments[6]:
                if c6 == c1 or c6 == c2 or c6 == c3 or c6 == c4 or 
                c6 == c5:
                  continue
                
                for c7 in segments[7]:
                  if c7 == c1 or c7 == c2 or c7 == c3 or c7 == c4 or 
                  c7 == c5 or c7 == c6:
                    continue
                  
                  values[0] = c1 & c2 & c3 & c5 & c6 & c7
                  values[1] = c3 & c6
                  values[2] = c1 & c3 & c4 & c5 & c7
                  values[3] = c1 & c3 & c4 & c6 & c7
                  values[4] = c2 & c3 & c4 & c6
                  values[5] = c1 & c2 & c4 & c6 & c7
                  values[6] = c1 & c2 & c4 & c5 & c6 & c7
                  values[7] = c1 & c3 & c6
                  values[8] = c1 & c2 & c3 & c4 & c5 & c6 & c7
                  values[9] = c1 & c2 & c3 & c4 & c6 & c7
                  
                  for i in 0..9:
                    values[i] = values[i].sorted(system.cmp[char]).join()
                  
                  combinations.add(values)
    
    # Now search for a combination that satisfies all the ten inputs
    # Once found, use it to calculate the four digit number and the sum
    var
      found: bool
      number: string
    
    for combination in combinations:
      found = true
      
      for pattern in reading.patterns:
        if combination.find(pattern) == -1:
          found = false
          break
      
      if found:
        for digit in reading.digits:
          number &= $combination.find(digit)
        total += number.parseInt()
        break    
  
  echo "Part 2: ", total