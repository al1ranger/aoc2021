# https://adventofcode.com/2021/day/10

# Use a stack to store the opening characters of the chunks.
# In Nim, stacks can be created using the deques module

import os, strutils, deques, algorithm

var
  lines: seq[string]
  incomplete: seq[Deque[char]] = @[]
  
let
  closingChars: array[0..3, char] = [')', ']', '}', '>']


block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  lines = readFile(os.paramStr(1)).strip().splitLines()

# Corrupted chunks score
# When an closing character is found, pop the top of the stack and compare the
# opening character to determine whether the chunk is valid or corrupt.
block part1:
  var
    stack: Deque[char]
    corrupt: bool
    score: int
  
  for line in lines:
    stack = initDeque[char]()
    corrupt = false
    
    for character in line:
      if character notin closingChars:
        stack.addLast(character)
      else:
        # If the opening character is incorrect => chunk is corrupt
        case stack.popLast():
          of '{':
            if character != '}':
              corrupt = true
          of '[':
            if character != ']':
              corrupt = true
          of '(':
            if character != ')':
              corrupt = true
          of '<':
            if character != '>':
              corrupt = true
          else:
            discard
      
      if corrupt:
        case character:
          of ')':
            score.inc(3)
          of ']':
            score.inc(57)
          of '}':
            score.inc(1197)
          of '>':
            score.inc(25137)
          else:
            discard
        break
    
    # For incomplete lines, the stack will not be empty
    if not corrupt and stack.len() > 0:
      incomplete.add(stack)
      
    
  echo "Part 1: ", score

# Autocomplete score
# Pop each character from the incomplete stacks stored earlier. Determine the
# closing character and calculate the score.
block part2:
  var
    scores: seq[int] = newseq[int](incomplete.len())
  
  for i in 0..<scores.len():
    scores[i] = 0
    
    while incomplete[i].len() > 0:
      scores[i] *= 5
      
      case incomplete[i].popLast():
        of '(':
          scores[i].inc(1)
        of '[':
          scores[i].inc(2)
        of '{':
          scores[i].inc(3)
        of '<':
          scores[i].inc(4)
        else:
          discard
    
  scores.sort()
  
  # There will always be an odd number of incomplete lines and as the sequence
  # index starts from 0, the last index will be double that of the middle 
  echo "Part 2: ", scores[scores.high().div(2)]