# https://adventofcode.com/2021/day/9

import os, strutils, algorithm


type
  HeightMap = seq[seq[int]]
  
  Point = tuple
      row, col: int
  
var
  heightMap: HeightMap = @[]
  lowPoints: seq[Point] = @[]
  
block readInput:
  if os.paramCount() < 1:
    quit(1)
    
  var
    row: seq[int]
    
  for line in readFile(os.paramStr(1)).strip().splitLines():
    row = newSeq[int](line.strip().len())
    
    for i in row.low()..row.high():
      row[i] = line[i].ord() - ord('0')
    
    heightMap.add(row)

# Sum of risk levels of low points    
block part1:
  var
    riskLevel: int
    left, right, top, bottom: int
    
  for row in heightMap.low()..heightMap.high():
    for col in heightMap[row].low()..heightMap[row].high():
      # As values can be from 0 to 9, 10 is used to indicate missing value
      left = 10
      top = 10
      right = 10
      bottom = 10
      
      if row > heightMap.low():
        if heightMap[row][col] >= heightMap[row - 1][col]:
          continue
      
      if row < heightMap.high():
        if heightMap[row][col] >= heightMap[row + 1][col]:
          continue
      
      if col > heightMap[row].low():
        if heightMap[row][col] >=  heightMap[row][col - 1]:
          continue
      
      if col < heightMap[row].high():
        if heightMap[row][col] >= heightMap[row][col + 1]:
          continue
      
      riskLevel += heightMap[row][col] + 1
      lowPoints.add((row, col))
  
  echo "Part 1: ", riskLevel

# Product of the three largest basin sizes
block part2:
  var
    basinSizes: seq[int] = newSeq[int](lowPoints.len())
    pointsToCheck: seq[Point]
    
  for i in lowPoints.low()..lowPoints.high():
    pointsToCheck = @[lowPoints[i]] # Start from the low point
    basinSizes[i] = 0
    
    while pointsToCheck.len() > 0:
      var
        currentPoint: Point
        
      currentPoint = pointsToCheck[0]
      pointsToCheck.del(0)
      
      # As there is no existence check while storing points, there is a chance
      # the same point was added more than once
      if heightMap[currentPoint.row][currentPoint.col] > 9:
        continue
      
      basinSizes[i].inc() # For the current point
      
      # Ensure that point is not counted again
      heightMap[currentPoint.row][currentPoint.col].inc(10)
      
      # Check the four neighbouring points and add those that belong
      if currentPoint.row > heightMap.low() and 
        heightMap[currentPoint.row - 1][currentPoint.col] < 9:
        pointsToCheck.add((currentPoint.row - 1, currentPoint.col))
          
      if currentPoint.row < heightMap.high() and
        heightMap[currentPoint.row + 1][currentPoint.col] < 9:
        pointsToCheck.add((currentPoint.row + 1, currentPoint.col))
          
      if currentPoint.col > heightMap[currentPoint.row].low() and
        heightMap[currentPoint.row][currentPoint.col - 1] < 9:
        pointsToCheck.add((currentPoint.row, currentPoint.col - 1))
      
      if currentPoint.col < heightMap[currentPoint.row].high() and
        heightMap[currentPoint.row][currentPoint.col + 1] < 9:
        pointsToCheck.add((currentPoint.row, currentPoint.col + 1))
  
  basinSizes.sort(SortOrder.Descending)
  echo "Part 2: ", basinSizes[0] * basinSizes[1] * basinSizes[2]