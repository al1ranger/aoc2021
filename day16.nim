# https://adventofcode.com/2021/day/16

import os, strutils

type
  Packet = object
    version, typeID, lengthTypeID: int
    value: int
    subPackets: seq[Packet]

# Calculate the value for the packet
proc calculatePacketValue(packet: var Packet) =
  # Determine values of the sub packets
  for subPacket in packet.subPackets.low()..packet.subPackets.high():
    packet.subPackets[subPacket].calculatePacketValue()
  
  case packet.typeID:
    of 0: # Sum
      packet.value = 0
      for subPacket in packet.subPackets:
        packet.value.inc(subPacket.value)
    of 1: # Product
      packet.value = 1
      for subPacket in packet.subPackets:
        packet.value *= subPacket.value
    of 2: # Minimum
      var
        values: seq[int] = @[]
      
      for subPacket in packet.subPackets:
        values.add(subPacket.value)
      
      packet.value = min(values)
    of 3: # Maximum
      var
        values: seq[int] = @[]
      
      for subPacket in packet.subPackets:
        values.add(subPacket.value)
      
      packet.value = max(values)
    of 5: # Greater than
      if packet.subPackets[0].value > packet.subPackets[1].value:
        packet.value = 1
    of 6: # Lesser than
      if packet.subPackets[0].value < packet.subPackets[1].value:
        packet.value = 1
    of 7: # Equals
      if packet.subPackets[0].value == packet.subPackets[1].value:
        packet.value = 1
    else:
      discard

# Builds a packet from the input string
proc readPacket(input: string, startIndex: var int): Packet =
  result = Packet()
  
  # Version
  result.version = fromBin[int](input[startIndex..startIndex + 2])
  startIndex.inc(3)
  
  # Type
  result.typeID = fromBin[int](input[startIndex..startIndex + 2])
  startIndex.inc(3)
  
  result.lengthTypeID = -1
  result.value = 0
  result.subPackets = @[]
  
  case result.typeID:
    of 4: # Single value
      var
        hasMore: bool
        valueStr: string
      
      # Read in blocks of 5 and use the first one to determine end of input
      hasMore = true
      while hasMore:
        if input[startIndex] == '0':
          hasMore = false
        
        valueStr &= input[startIndex+1..startIndex+4]
        startIndex.inc(5)
      
      result.value = fromBin[int](valueStr)
    else: # Operator packet
      # Length type
      result.lengthTypeID = fromBin[int]($input[startIndex])
      startIndex.inc(1)
      
      case result.lengthTypeID:
        of 0: # Length of packets is specified
          var
            subPacketsData: string
            subPackets, index: int
          
          subPackets = fromBin[int](input[startIndex..startIndex + 14])
          startIndex.inc(15)
          
          subPacketsData = input[startIndex..startIndex+subPackets-1]
          index = subPacketsData.low()
          while index < subPacketsData.high():
            result.subPackets.add(subPacketsData.readPacket(index))
            
          startIndex.inc(subPacketsData.len())
        of 1: # Number of packets is specified
          var
            subPackets, index: int
            
          subPackets = fromBin[int](input[startIndex..startIndex + 10])
          startIndex.inc(11)
          
          index = startIndex
          while subPackets > 0:
            result.subPackets.add(input.readPacket(index))
            
            subPackets -= 1
          
          startIndex = index
        else:
          discard
  
  # Calculate packet value        
  result.calculatePacketValue()

proc getVersionSum(packet: Packet): int =
  result = packet.version
  
  for subPacket in packet.subPackets:
    result.inc(subPacket.getVersionSum())
  
    
var
  packet: Packet
  
block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  let
    data = readFile(os.paramStr(1)).strip().splitLines()
   
  var
    packetDataBin: string
    index: int = 0
    
  for c in data[0]:
    packetDataBin = packetDataBin & toBin(fromHex[int]($c), 4)
  
  packet = packetDataBin.readPacket(index)

# Sum of all versions
block part1:  
  echo "Part 1: ", packet.getVersionSum()

# Packet value  
block part2:
  echo "Part 2: ", packet.value