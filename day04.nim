# https://adventofcode.com/2021/day/4

import os, strutils

type
  Cell = tuple
    value: int
    selected: bool
  
  Board = object
    rows, rank: int
    grid: array[0..4, array[0..4, Cell]]
  
  Boards = seq[Board]

proc reset(board: var Board) =
  for row in 0..4:
    for col in 0..4:
      board.grid[row][col].selected = false

proc addRow(board: var Board, row: string) =
  if board.rows == 5:
    return
  
  # Single digit values are padded with a space
  let
    values: seq[string] = row.strip().replace("  ", " ").split(' ')
  
  for col in 0..4:
    board.grid[board.rows][col].value = values[col].parseInt()
  
  board.rows += 1  

proc select(board: var Board, value: int) =
  for row in 0..4:
    for col in 0..4:
      if board.grid[row][col].value == value:
        board.grid[row][col].selected = true
        return

proc checkWin(board: Board): bool =
  # Check rows
  for row in 0..4:
    result = true
    for col in 0..4:
      result = result and board.grid[row][col].selected
    if result:
      return
  
  # Check columns
  for col in 0..4:
    result = true
    for row in 0..4:
      result = result and board.grid[row][col].selected
    if result:
      return

proc calculateScore(board: Board, multiplier: int): int =
  result = 0
  
  if multiplier == 0:
    return
  
  # Sum of unselected cells
  for row in 0..4:
    for col in 0..4:
      if not board.grid[row][col].selected:
        result += board.grid[row][col].value
  
  # Multiply to get the final score
  result *= multiplier

proc reset(boards: var Boards) =
  for i in boards.low() .. boards.high():
    boards[i].reset()

var
  numbers: seq[int] = @[]
  boards: Boards = @[]

block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  let 
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  for line in data:
    if numbers.len() == 0: # Initialize sequence in which numbers will be called
      for value in line.strip().split(","):
        numbers.add(value.parseInt())
      continue
    
    if line == "": # Start of a new board
      boards.add(Board())
      continue
    
    boards[boards.len() - 1].addRow(line)
  
block part1:
  for number in numbers:
    for i in 0..<boards.len():
      # Set selection
      boards[i].select(number)
      
      # Check for win
      if boards[i].checkWin():
        echo "Part 1: ", boards[i].calculateScore(number)
        break part1

block part2:
  boards.reset()
  
  var
    rank: int = 1
  
  for number in numbers:
    for i in 0..<boards.len():
      # Check if board has won
      if boards[i].rank > 0:
        continue
      
      # Set selection.
      boards[i].select(number)
      
      # Set rank if board has won
      if boards[i].checkWin():
        boards[i].rank = rank
        rank += 1
        
        # Calculate score if last to win
        if rank > boards.len():
          echo "Part 2: ", boards[i].calculateScore(number)
          break part2