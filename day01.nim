# https://adventofcode.com/2021/day/1

import os, strutils

if os.paramCount() < 1:
  quit(1)

let data = readFile(os.paramStr(1)).strip().splitLines()

# Simple comparison
block part1:
  var
    currMeas, prevMeas: int = 999999
    increaseCount: int
  
  for line in data:
    currMeas = line.parseInt()
    if currMeas > prevMeas:
      increaseCount += 1
  
    prevMeas = currMeas
  
  echo "Part 1: ", increaseCount

# Window based comparison  
block part2:
  var
    readings: seq[int] = newSeq[int]()
    previousSum, currentSum, increaseCount: int
  
  previousSum = 999999
  for line in data:
    readings.add(line.parseInt())
    
    if readings.len() == 3:
      currentSum = readings[0] + readings[1] + readings[2]
      
      if currentSum > previousSum:
        increaseCount += 1
      
      previousSum = currentSum
      
      # Discard the oldest
      readings[0] = readings[1]
      readings[1] = readings[2]
      readings.del(2)
  
  echo "Part 2: ", increaseCount
  