# https://adventofcode.com/2021/day/6
# Useful hints avaiable at https://www.reddit.com/r/adventofcode/comments/r9z49j/2021_day_6_solutions/

import os, strutils

# Brute force or recursive algorithms will fail/be very slow for part 2.
# This is a common theme in most AoC challenges.

# For this reason, we need to simplify the calculation, which can be done as:
#   1. There can be only 9 possible values for the counter (0 to 8)
#   2. Every day, the number of fish under a particular count moves to the 
#      previous count
#   3. An array of 9 elements can be used to keep track of the number of fish
#      where the counter is at a specific value
#   4. When the counter reaches 0, the number of fish is added to those whose
#      counter is 6 and and also set as those whose counter is 8

proc simulateGrowth(fish: seq[int], days: int): int =
  result = 0

  var
    # The counter of days till the fish spawns can only have 9 values
    counts: array[0..8, int] = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    newFish: int

  # Initialize
  for thisFish in fish:
    counts[thisFish] += 1

  for day in 1..days:
    newFish = 0

    for count in 0..<8:
      if count == 0: # These will spawn new fish
        newFish = counts[count]
      
      counts[count] = counts[count + 1] # Decrement the counter
      
      if count == 7:
        counts[count + 1] = newFish # The newly spawned fish
        counts[6] += newFish # Counter is reset to 6 from 0
  
  # Calculate the sum
  for count in 0..8:
    result += counts[count]

var
  fish: seq[int] = @[]

block readInput:
  if os.paramCount() < 1:
    quit(1)

  let 
    data = readFile(os.paramStr(1)).strip().splitLines()[0]

  # Initial timer values
  for value in data.strip().split(","):
    fish.add(value.parseInt())

# 80 day simulation
block part1:
  echo "Part 1: ", fish.simulateGrowth(80)

# 256 day simulation
block part2:
  echo "Part 2: ", fish.simulateGrowth(256)