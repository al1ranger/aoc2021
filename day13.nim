# https://adventofcode.com/2021/day/13

import os, strutils

var
  paper: seq[string] = @[]
  foldInstructions: seq[string] = @[]

# Carries out a folding instruction and returns the resulting paper grid
proc fold(paper: seq[string], instruction: string): seq[string] =
  var
    parts: seq[string]
    newRows, newCols, x, y: int
  
  result = @[]
  
  # Fold along X axis => fold left, fold along Y axis => fold up
  parts = instruction.split("=")
  case parts[0]:
    of "x":
      newCols = parts[1].parseInt()
      newRows = paper.len()
    of "y":
      newRows = parts[1].parseInt()
      newCols = paper[0].len()
    else:
      return
  
  # Initialize the new paper. The co-ordinate along which folding happens does  
  # not have any dots and thus can be excluded the final grid.
  for row in 1..newRows:
    result.add(".".repeat(newCols))
  
  for row in paper.low()..paper.high():
    # Calculate the new Y co-ordinate
    if row > newRows:
      y = newRows - (row - newRows)
    else:
      y = row
    
    for col in paper[row].low()..paper[row].high():
      # Blank positions can be ignored
      if paper[row][col] == '.':
        continue
      
      # Calculate the new X co-ordinate
      if col > newCols:
        x = newCols - (col - newCols)
      else:
        x = col 
      
      # Set the dot 
      result[y][x] = paper[row][col]

block readInput:
  if os.paramCount() < 1:
    quit(1)
    
  let
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  var
    x, y, maxX, maxY: int
    values: seq[string]
    readFoldInstructions: bool
    
  # First pass to determine max X and Y
  for line in data:
    if line == "": # Folding instructions
      break
    
    values = line.split(",")
    x = values[0].parseInt()
    y = values[1].parseInt()
    
    if x > maxX:
      maxX = x
    
    if y > maxY:
      maxY = y
  
  # Initialize the paper grid
  for row in 0..maxY:
    paper.add(".".repeat(maxX + 1)) # As maxX must be the last index
  
  # Second pass to set the dots and read the folding instructions
  readFoldInstructions = false
  for line in data:
    if line == "":
      readFoldInstructions = true
      continue
      
    if not readFoldInstructions:
      values = line.split(",")
      x = values[0].parseInt()
      y = values[1].parseInt()
      paper[y][x] = '#'
    else:
      foldInstructions.add(line[11..line.high()]) # Ignore text 'fold along '

# Visible dots after the first fold
block part1:
  var
    dots: int
  
  paper = paper.fold(foldInstructions[0])
  
  for line in paper:
    dots.inc(line.count("#"))
  
  echo "Part 1: ", dots

# Complete the folding and print the output to view the letters
block part2:
  for i in 1..foldInstructions.high():
    paper = paper.fold(foldInstructions[i])
    
  echo "Part 2:"
  for line in paper:
    echo line