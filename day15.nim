# https://adventofcode.com/2021/day/15
# Useful hints at
# https://www.reddit.com/r/adventofcode/comments/rgqzt5/2021_day_15_solutions/
# https://en.wikipedia.org/wiki/Dijkstra's_algorithm

import os, strutils

type
  Point = tuple
    x, y: int
  
  Cavern = seq[seq[int]]

# Gets the risk of goint to x, y
proc getRisk(travelRisks: Cavern, x, y: int): int =
  result = 0
  
  # This will be true for part 1
  if x >= travelRisks.low() and x <= travelRisks.high() and 
    y >= travelRisks.low() and y <= travelRisks.high():
    result = travelRisks[y][x]
    return
  
  # This logic is for part 2  
  var
    cavernX, cavernY, xInc, yInc: int
  
  cavernX = x mod travelRisks.len()
  xInc = x div travelRisks.len()
  cavernY = y mod travelRisks.len()
  yInc = y div travelRisks.len()
  
  # Below statement is equivalent to
  # for i in 1..(xInc + yInc):
  #   result.inc()
  #   if result > 9:
  #     result = 1
  result = ((travelRisks[cavernY][cavernX] + xInc + yInc - 1) mod 9) + 1
  

# Finds the lowest risk for getting to all points in the array        
proc calculateLowestRisks(risks: var Cavern, riskValues: Cavern) =
  var
    hasChanges: bool
    currentPoint: Point
    currentCost: int
  
  # Here we scan the whole grid, updating the cost of each node based on
  # its neighbors. When no updates happen during a pass, it means that the
  # minimum costs have been found for all the nodes.
  # We can get away with using this approach for this problem as the path is
  # not required.
  
  hasChanges = true
  while hasChanges:
    
    hasChanges = false
    for y in risks.low()..risks.high():
      for x in risks.low()..risks.high():
        currentPoint = (x, y)
        
        for dy in -1..1:
          for dx in -1..1:
            # We need to check only left, right, top and bottom
            if dy.abs() == dx.abs():
              continue
            
            var
              neighbor: Point
            
            neighbor = (currentPoint.x + dx, currentPoint.y + dy)
            
            # Check neighbor is valid
            if neighbor.x < risks.low() or neighbor.x > risks.high() or
              neighbor.y < risks.low() or neighbor.y > risks.high():
              continue
            
            currentCost = risks[currentPoint.y][currentPoint.x]
            currentCost.inc(riskValues.getRisk(neighbor.x, neighbor.y))
            if currentCost < risks[neighbor.y][neighbor.x]:
              risks[neighbor.y][neighbor.x] = currentCost
              hasChanges = true

# Initializes the risks as 0 for start and int.high() everywhere else
proc initializeRiskCosts(risks: var Cavern, size: int) =
  risks = newSeq[seq[int]](size)
  
  for row in risks.low()..risks.high():
    risks[row] = newSeq[int](size)
    
    for col in risks.low()..risks.high():
      if row == 0 and col == 0:
        risks[row][col] = 0
      else:
        risks[row][col] = int.high()
  
var
  riskMap: Cavern = @[]

block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  for line in readFile(os.paramStr(1)).strip().splitLines():
    riskMap.add(@[])
    for digit in line:
      riskMap[riskMap.high()].add(parseInt($digit))
  
# Path with lowest risk for grid equal to puzzle input
block part1:
  var
    risks: Cavern = @[]
  
  risks.initializeRiskCosts(riskMap.len())
  risks.calculateLowestRisks(riskMap)
  
  echo "Part 1: ", risks[risks.high()][risks.high()]

# Path with lowest risk for grid five times the puzzle input
block part2:
  var
    risks: Cavern = @[]  
  
  risks.initializeRiskCosts(5 * riskMap.len())
  risks.calculateLowestRisks(riskMap)
  
  echo "Part 2: ", risks[risks.high()][risks.high()]