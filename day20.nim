# https://adventofcode.com/2021/day/20
# Useful hints at 
# https://www.reddit.com/r/adventofcode/comments/rkf5ek/2021_day_20_solutions/

# Here, it is important to understand that for the infinite pixels that are not
# present in the array
# 1. All pixels will be of the same state, i.e. either on or off
# 2. If they are on, the state for the next step will be determined by the last
#    character in the enhancement algorithm (binary 111111111 = 511)
# 3. If they are off, the state for the next step will be determined by the 
#    first character in the enhancement algorithm

import os, strutils

type
  Image = object
    data: seq[seq[char]]
    indexForMissingPixels: int

# Gets the new index in the algorithm that will be used for missing pixels
proc getNewIndexForMissingPixels(algorithm: string, currentIndex: int = 0): int 
  {.inline.} =
  
  # For the missing pixels, all the neighbouring pixels will be the same,
  # which means that the index into the algorithm array can be either 0 or 511.
  # Here we check the value in the algorithm array at the current index to
  # determine what to return.
  
  if algorithm[currentIndex] == '#': # All bits 1
    result = algorithm.high()
  else: # All bits 0
    result = algorithm.low()

# Carries out one step of the enhancement
proc enhance(input: Image, algorithm: string): Image =
  # As the state of pixels immediately adjacent to the exterior of the input
  # can be determined, the dimensions increase by 2.
  # Here essentially, we are shifting the origin of the co-ordinate system such 
  # that position [0][0] in the input corresponds to [1][1] in the output.
  
  result = Image()
  result.data = newSeq[seq[char]](input.data.len() + 2)
  result.indexForMissingPixels = 
    algorithm.getNewIndexForMissingPixels(input.indexForMissingPixels)
    
  # Carry out the enhancement
  for row in result.data.low()..result.data.high():
    var
      inputRow: int
      
    inputRow = row - 1 # Convert to row number in the input array
    result.data[row] = newSeq[char](input.data.len() + 2)
    
    for col in result.data[row].low()..result.data[row].high():
      var
        algorithmIdxBinary: string
        srcRow, srcCol, inputCol: int
      
      inputCol = col - 1 # Convert to column number in the input array
      algorithmIdxBinary = ""
      
      # Build the binary string using the 3 by 3 grid
      for dRow in -1..1:
        srcRow = inputRow + dRow
        
        if srcRow < input.data.low() or srcRow > input.data.high():
          # There are three columns in a row
          if algorithm[input.indexForMissingPixels] == '#':
            algorithmIdxBinary &= "111"
          else:  
            algorithmIdxBinary &= "000"
        else:
          for dCol in -1..1:
            srcCol = inputCol + dCol # Column in input
          
            if srcCol < input.data[srcRow].low() or 
              srcCol > input.data[srcRow].high():
              if algorithm[input.indexForMissingPixels] == '#':
                algorithmIdxBinary &= "1"
              else:
                algorithmIdxBinary &= "0"
            else:
              if input.data[srcRow][srcCol] == '#':
                algorithmIdxBinary &= "1"
              else:
                algorithmIdxBinary &= "0"
      
      # Set the pixel in the output    
      result.data[row][col] = algorithm[fromBin[int](algorithmIdxBinary)]

# Gets the count of lit pixels in an image
proc getLitPixels(image: Image): int =
  result = 0
  
  for line in image.data:
    for pixel in line:
      if pixel == '#':
        result.inc()
      
var
  enhancementAlgorithm: string
  image: Image

block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  image = Image()
  image.data = @[]
  
  for line in readFile(os.paramStr(1)).strip().splitLines():
    if line == "":
      continue
    
    if enhancementAlgorithm == "":
      enhancementAlgorithm = line
      continue
      
    image.data.add(newSeq[char](line.len()))
    for col in 
      image.data[image.data.high()].low()..image.data[image.data.high()].high():
      image.data[image.data.high()][col] = line[col]
  
  image.indexForMissingPixels = 
    enhancementAlgorithm.getNewIndexForMissingPixels()

# Image after two enhancements
block part1:
  for step in 1..2:
    image = image.enhance(enhancementAlgorithm)
        
  echo "Part 1: ", image.getLitPixels()

# Image after fifty enhancements
block part2:
  for step in 1..48:
    image = image.enhance(enhancementAlgorithm)
    
  echo "Part 2: ", image.getLitPixels()