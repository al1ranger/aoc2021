# https://adventofcode.com/2021/day/5

import os, strutils

type
  Point = tuple
    x, y: int
  
  Line = object
    start, finish: Point

  Grid = seq[seq[int]]

var
  lines: seq[Line] = @[]
  grid: Grid

block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  let 
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  var
    coords: seq[string]
    xyPair: seq[string]
    gridSize: int = 0
    line: Line

  for inputLine in data:
    line = Line()

    coords = inputLine.strip().split(" -> ")
    
    xyPair = coords[0].split(",")
    line.start = (x: xyPair[0].parseInt(), y: xyPair[1].parseInt())
    
    if line.start.x > gridSize:
      gridSize = line.start.x
    if line.start.y > gridSize:
      gridSize = line.start.y

    xyPair = coords[1].split(",")
    line.finish = (x: xyPair[0].parseInt(), y: xyPair[1].parseInt())
    
    if line.finish.x > gridSize:
      gridSize = line.finish.x
    if line.finish.y > gridSize:
      gridSize = line.finish.y

    lines.add(line)
  
  # Initialize a square grid
  for i in 0..gridSize:
    grid.add(newSeq[int](gridSize + 1)) # gridSize has to be the max index

# Only horizontal or vertical lines  
block part1:
  var
    twoOrMore: int = 0

  for line in lines:
    # Ignore diagonal lines
    if line.start.x != line.finish.x and line.start.y != line.finish.y:
      continue

    if line.start.x == line.finish.x: # Vertical line
      for y in min(line.start.y, line.finish.y) .. max(line.start.y, line.finish.y):
        grid[y][line.start.x] += 1
    elif line.start.y == line.finish.y: # Horizontal line
      for x in min(line.start.x, line.finish.x) .. max(line.start.x, line.finish.x):
        grid[line.start.y][x] += 1
  
  # Count
  for row in grid.low()..grid.high():
    for col in grid.low()..grid.high():
      if grid[row][col] >= 2:
        twoOrMore += 1
  
  echo "Part 1: ", twoOrMore

# Update for daigonal lines
block part2:
  var
    dx, dy, steps: int
    twoOrMore: int = 0

  for line in lines:
    # Horizontal and vertical lines have already been considered
    if line.start.x == line.finish.x or line.start.y == line.finish.y:
      continue

    # Diagonal lines are at 45 degrees which means that the number of steps remains same for x and y
    # Start and finish values of the co-ordinate will decide the sign
    steps = abs(line.start.x - line.finish.x)

    if line.finish.x > line.start.x:
      dx = 1
    else:
      dx = -1
    
    if line.finish.y > line.start.y:
      dy = 1
    else:
      dy = -1  

    for i in 0 .. steps:
      grid[line.start.y + i * dy][line.start.x + i * dx] += 1

  # Count  
  for row in grid.low()..grid.high():
    for col in grid.low()..grid.high():
      if grid[row][col] >= 2:
        twoOrMore += 1
  
  echo "Part 2: ", twoOrMore