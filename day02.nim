# https://adventofcode.com/2021/day/2

import os, strutils

if os.paramCount() < 1:
  quit(1)

let data = readFile(os.paramStr(1)).strip().splitLines()

# Simple movement
block part1:
  var
    hPos, depth, value: int
    commands: seq[string]
    
  for line in data:
    commands = line.split(" ")
    value = commands[1].parseint()
    
    case commands[0]:
      of "forward":
        hPos += value
      of "down":
        depth += value
      of "up":
        depth -= value
      else:
        discard
    
  echo "Part 1: ", depth * hPos
  
  # Aim based movement
  block part1:
    var
      hPos, depth, value, aim: int
      commands: seq[string]
      
    for line in data:
      commands = line.split(" ")
      value = commands[1].parseint()
      
      case commands[0]:
        of "forward":
          hPos += value
          depth += (aim * value)
        of "down":
          aim += value
        of "up":
          aim -= value
        else:
          discard
      
    echo "Part 2: ", depth * hPos
