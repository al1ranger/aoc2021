# https://adventofcode.com/2021/day/17
# Useful hints at: 
# https://www.reddit.com/r/adventofcode/comments/ri9kdq/2021_day_17_solutions/

import os, strutils, strscans

var
  xMin, xMax, yMin, yMax: int

# Here x co-ordinate is positive and y co-ordinate is negative. The first 
# co-ordinate is always the minimum.
block readInput:
  if os.paramCount() < 1:
    quit(1)
    
  let data = readFile(os.paramStr(1)).strip().splitLines()
  discard scanf(data[0], "target area: x=$i..$i, y=$i..$i", 
    xMin, xMax, yMin, yMax)
  

# Maximum height
block part1:
  # Here, the maximum height that must be present at y = 0 is abs(yMin) - 1,
  # so that the y co-ordinate of the probe will be within the range after 
  # velocity increases by 1 due to drag. As the acceleration is always 1 unit
  # downwards, the height is (abs(yMin) - 1) + (abs(yMin) - 1) - 1) + ... + 1,
  # or sum of the first (abs(yMin) - 1) natural numbers.
  
  # When using the equations of physics to solve, it is important to keep in 
  # mind that the initial velocity for the descent is -1 and not 0, as per the
  # rules of acceleration that are mentioned.
  
  var
    v: int
  
  v = abs(yMin) - 1
  
  echo "Part 1: ", v * (v + 1) / 2

# All possible combinations  
block part2:
  # Here maximum start velocities cannot exceed the outer limits of the region
  # Due to the rules of acceleration, start velocity can be 0
  var
    vx, vy, hits: int
    
  hits = 0
  
  # As probe has to move to the right, velocity along x cannot be negative
  # For y direction, the maximum downward velocity can be maxY and maximum 
  # upward velocity as maxY - 1, as obtained for part 1
  for vx in 0..xMax:
    for vy in yMin..(-yMin):
      var
        x, y, ux, uy: int
        
      x = 0
      y = 0
      ux = vx
      uy = vy
      
      while true:
        # Check if further iterations are needed
        if x > xMax or y < yMin:
          break
        elif x >= xMin and x <= xMax and y >= yMin and y <= yMax:
          hits.inc()
          break
        
        # Update position and velocity
        x.inc(ux)
        y.inc(uy)
        
        if ux > 0:
          ux.dec()
        elif ux < 0:
          ux.inc()
        
        uy.dec()
        
  echo "Part 2: ", hits