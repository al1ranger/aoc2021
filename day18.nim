# https://adventofcode.com/2021/day/18

import os, strutils, math, algorithm, pegs

type
  NumberDetails = tuple
    value: string
    start, finish: int

var
  numbers: seq[string] = @[]
  
block readInput:
  if os.paramCount() < 1:
    quit(1)
    
  numbers = readFile(os.paramStr(1)).strip().splitLines()

# Gets the first number from the string
proc getFirstNumber(number: string, greaterThan: int = -1, 
  reversed: bool = false): NumberDetails =
  result = ("", -1, -1)
  
  var
    index: int
  
  for i in number.low()..number.high():
    if reversed:
      index = number.high() - i
    else:
      index = i
    
    if number[index] >= '0' and number[index] <= '9':
      result.value &= number[index]
      if result.start < 0:
        result.start = index
      result.finish = index
    elif result.value.len() > 0:
      if result.value.parseInt() > greaterThan:
        break
      else:
        result.value = ""
        result.start = -1
        result.finish = -1
    
    # The number was stored in reverse, which needs to be corrected
    if reversed:
      result.value.reverse()
      result.start.swap(result.finish)

# Performs the explode operation
proc explode(number: var string): bool =
  result = false
  
  var
    level, startPos, endPos: int
    before, after, toExplode: string
    
  # Locate the first number four or more levels deep 
  level = 0
  startPos = 0
  endPos = 0
  
  for i in number.low()..number.high():
    if number[i] == '[':
      startPos = i
      level.inc()
    elif number[i] == ']':
      endPos = i
      level.dec()
      if level >= 4:
        break
  
  # Check if a match is found
  if level < 4:
    return
    
  result = true

  # Break the string into three parts for processing
  before = number[number.low()..<startPos]
  toExplode = number[startPos..endPos]
  after = number[(endPos + 1)..number.high()]
  
  var
    numLeftOld, numRightOld: NumberDetails
    numLeftNew, numRightNew, lhs, rhs: string
    comma: int
  
  # Get numbers to the left and right that are to be replaced
  comma = toExplode.find(',')
  lhs = toExplode[1..<comma]
  rhs = toExplode[comma+1..<toExplode.high()]
  
  numLeftOld = before.getFirstNumber(-1, true)
  numRightOld = after.getFirstNumber()
  
  # Replace the left and right numbers
  if numLeftOld.value != "":
    numLeftNew = $(numLeftOld.value.parseInt() + lhs.parseInt())
    before = before[before.low()..<numLeftOld.start] & numLeftNew &
      before[(numLeftOld.finish + 1)..before.high()]
    
  if numRightOld.value != "":
    numRightNew = $(numRightOld.value.parseInt() + rhs.parseInt())
    after = after[after.low()..<numRightOld.start] & numRightNew &
      after[(numRightOld.finish + 1)..after.high()]
  
  # Build the final exploded string
  number = before & "0" & after

# Performs the split operation
proc split(number: var string): bool =
  result = false
  
  var
    replacementPair: string
    srcNum: int
    numberToSplit: NumberDetails
    
  # Get the first number that is 10 or greater  
  numberToSplit = number.getFirstNumber(9)
  
  if numberToSplit.value == "":
    return
  
  result = true
  
  # Split the number
  srcNum = numberToSplit.value.parseInt()
  replacementPair = '[' & $(srcNum.floorDiv(2)) & ',' & 
    $(srcNum.ceilDiv(2))  & ']'
  
  # Replace with the new value
  number = number[number.low()..<numberToSplit.start] & replacementPair &
    number[(numberToSplit.finish + 1)..number.high()]

# Reduces a number
proc reduce(number: var string) =
  # If an explosion happens, no split is to be performed and the next iteration
  # should begin
  while true:
    if number.explode():
      continue
    
    if number.split():
      continue
    
    # No explode or split, so exit
    break

# Gets the magnitude of a number
proc getMagnitude(number: string): string =
  result = ""
  
  var 
    matched: bool
  
  # Callback function for PEG matches
  # See https://nim-lang.org/docs/pegs.html
  proc handleMatches(m: int, n: int, c: openArray[string]): string =
    result = ""
    
    var
      lhs, rhs: int
    
    # Set flag for the next iteration
    matched = true
    
    # Calculate the value of the matched pair
    if n > 0:
      lhs = c[0].parseInt()
    else:
      lhs = 0
    
    if n > 1:
      rhs = c[1].parseInt()
    else:
      rhs = 0
    
    result = $(3 * lhs + 2 * rhs)
    
  # Repeatedly process the number till no matches remain
  matched = true
  result = number
  while matched:
    matched = false
    result = result.replace(peg"'['{\d+}','{\d+}']'", handleMatches)

# Sum of all numbers
block part1:
  var
    sum: string
  
  # Calculate the sum
  for number in numbers:
    if sum == "":
      sum = number
    else:
      sum = '[' & sum & ',' & number & ']'
    sum.reduce()
  
  echo "Part 1: ", sum.getMagnitude()

# Maximum magnitude of sum of any two numbers
block part2:
  var
    magnitude, maxMagnitude: int
    sum: string
  
  # Here both permutations need to be checked  
  for i in numbers.low()..numbers.high():
    for j in numbers.low()..numbers.high():
      if i == j:
        continue
      
      for k in 1..2:
        if k == 1:
          sum = '[' & numbers[i] & ',' & numbers[j] & ']'
        else:
          sum = '[' & numbers[j] & ',' & numbers[i] & ']'
      
        sum.reduce()
        magnitude = sum.getMagnitude().parseInt()
        if magnitude > maxMagnitude:
          maxMagnitude = magnitude
  
  echo "Part 2: ", maxMagnitude