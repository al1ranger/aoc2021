# https://adventofcode.com/2021/day/10

# Though this problem is straightforward, it is important to consider that the
# solution for part two might occur in less than 100 steps

import os, strutils

const
  GridSize: int = 100

type
  Grid = array[0..9, array[0..9, int]]
  
  StepResult = tuple
    flashes: int
    synchronized: bool
  
  Point = tuple
    row, col: int

var
  grid: Grid
  firstSynchronizedOn: int = 0
  currentStep: int = 1 
  currentState: StepResult

# Execute a step of the octopuses' energy level changes
proc executeStep(grid: var Grid): StepResult =
  result.flashes = 0
  result.synchronized = false
  
  # Update the energy level
  for row in grid.low()..grid.high():
    for col in grid[row].low()..grid[row].high():
      grid[row][col].inc()
    
  # Flash if energy level > 9
  for row in grid.low()..grid.high():
    for col in grid[row].low()..grid[row].high():
      if grid[row][col] > 9:
        var
          stepFlashes: seq[Point] # Sources of energy distribution
        
        stepFlashes = @[]
        stepFlashes.add((row, col))
        
        while stepFlashes.len() > 0:
          var
            currentPoint: Point
            neighbor: Point
          
          # Get the origin of energy distribution
          currentPoint = stepFlashes[0]
          stepFlashes.del(0)  
          
          # As there is no existence check while adding to the sequence,
          # there is a possibility that the current octopus has already flashed
          if grid[currentPoint.row][currentPoint.col] == 0:
            continue
          
          grid[currentPoint.row][currentPoint.col] = 0
          result.flashes.inc()
        
          # Propagate the energy to neighbors and check for flashes
          for i in -1..1:
            for j in -1..1:
              if i == 0 and j == 0: # This is the current point
                continue
              
              neighbor = (currentPoint.row + i, currentPoint.col + j)
              
              # Check if row and column of neighbor are valid
              if neighbor.row < grid.low() or neighbor.row > grid.high() or 
                neighbor.col < grid[row].low() or neighbor.col > grid[row].high():
                continue
              
              # Ignore if already flashed
              if grid[neighbor.row][neighbor.col] == 0:
                continue
              
              # Increase and energy and check for flash
              grid[neighbor.row][neighbor.col].inc()
              if grid[neighbor.row][neighbor.col] > 9:
                stepFlashes.add(neighbor)
    
  # Check if synchronized, i.e. all have flashed at the same time
  # This means that the number of flashes equals the size of the grid, as each
  # octopus can flash only once during the step
  if result.flashes == GridSize:
    result.synchronized = true

block readInput:
  if os.paramCount() < 1:
    quit(1)
  
  var 
    row: int = grid.low()
  
  for line in readFile(os.paramStr(1)).strip().splitLines():
    for i in grid[row].low()..grid[row].high():
      grid[row][i] = parseInt($line[i])
    row.inc()
    
    if row > grid.high():
      break
    
# Flashes after 100 steps    
block part1:
  var
    flashes: int = 0
  
  while currentStep < 101:
    currentState = grid.executeStep()
    flashes.inc(currentState.flashes)    
    
    if firstSynchronizedOn == 0 and currentState.synchronized:
      firstSynchronizedOn = currentStep
    
    currentStep.inc()
  
  echo "Part 1: ", flashes

# First step when flashes are synchronized
block part2:
  # Check from step 101, if synchronization did not occur in the previous steps
  while firstSynchronizedOn == 0:
    currentState = grid.executeStep()
    
    if currentState.synchronized:
      firstSynchronizedOn = currentStep
    
    currentStep.inc()
    
  echo "Part 2: ", firstSynchronizedOn